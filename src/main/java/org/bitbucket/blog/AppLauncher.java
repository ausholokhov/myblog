package org.bitbucket.blog;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.File;

@SpringBootApplication
public class AppLauncher {

    //    @Value("${image.folder.uploadDirPath}")
    public static String uploadDirPath = "upload-dir/";

    public static void main(String[] args) {
        SpringApplication.run(AppLauncher.class, args);
    }

    @Bean
    CommandLineRunner init() {
        return (String[] args) -> new File(uploadDirPath).mkdir();
    }
}
