package org.bitbucket.blog.controllers;

import org.bitbucket.blog.AppLauncher;
import org.bitbucket.blog.utils.FileSaver;
import org.springframework.stereotype.Controller;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;

/**
 * Created by Anton on 13.03.2016.
 */
@Controller
public class ImageController {


    @RequestMapping(value = "/image/{dir}/{date}/{imagename.+}")
    public void getImage(HttpServletResponse response,
                         @PathVariable("dir") String dir,
                         @PathVariable("date") String date,
                         @PathVariable("imagename.+") String imageName) throws IOException {
        String externalImagePath = dir + "/" + date + "/" + imageName;
        File file = new File(externalImagePath);
        InputStream inputStream = new BufferedInputStream(new FileInputStream(file));
        FileCopyUtils.copy(inputStream, response.getOutputStream());
    }

    @RequestMapping(value = "/saveImage", method = RequestMethod.POST)
    @ResponseBody
    public String saveImage(@RequestParam("img") MultipartFile img) throws IOException {
        String imageExt = img.getOriginalFilename().split("\\.")[1];
        return FileSaver.saveFile(img, AppLauncher.uploadDirPath, imageExt);
    }
}
