package org.bitbucket.blog.controllers;

import org.bitbucket.blog.AppLauncher;
import org.bitbucket.blog.models.Entry;
import org.bitbucket.blog.models.Tag;
import org.bitbucket.blog.models.User;
import org.bitbucket.blog.service.EntryService;
import org.bitbucket.blog.service.TagService;
import org.bitbucket.blog.service.UserService;
import org.bitbucket.blog.utils.FileSaver;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;
import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by Admin on 29.03.2016.
 */
@Controller
public class EntryController {

    @Autowired
    private EntryService entryService;
    @Autowired
    private UserService userService;
    @Autowired
    private TagService tagService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        binder.registerCustomEditor(Date.class,
                new CustomDateEditor(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS"), true));
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/addEntry", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity addEntry(@RequestParam(value = "image", required = false) MultipartFile img,
                                   @ModelAttribute Entry entry,
                                   Principal principal) {
        try {
            if (img != null) {
                String imageExt = img.getOriginalFilename().split("\\.")[1];
                String imagePath = FileSaver.saveFile(img, AppLauncher.uploadDirPath, imageExt);
                entry.setPreviewImage(imagePath);
            }
            User author = userService.findByLogin(principal.getName());
            entry.setAuthor(author);
            entryService.create(entry);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/deleteEntry", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity deleteEntry(@RequestParam("id") Long entryId) {
        try {
            entryService.delete(entryId);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/upEntry", method = RequestMethod.DELETE)
    @ResponseBody
    public ResponseEntity upEntry(@RequestParam("id") Long entryId) {
        try {
            Entry entry = entryService.get(entryId);
            entry.setPostDate(new Date());
            entryService.update(entry);
            return new ResponseEntity(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/editEntry", method = RequestMethod.GET)
    public ModelAndView editEntry(@RequestParam("id") Long entryId) {
        Entry entry = entryService.get(entryId);
        ModelAndView modelAndView = new ModelAndView("admin_page");
        modelAndView.addObject("entry", entry);
        List<Tag> tags = tagService.getAll();
        tags.removeAll(entry.getTags());
        modelAndView.addObject("tags", tags);
        return modelAndView;
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(value = "/preview", method = RequestMethod.POST)
    public ModelAndView entryPreview(@RequestParam(value = "image") MultipartFile img,
                                     @ModelAttribute Entry entry, Principal principal) throws IOException {
        String imageExt = img.getOriginalFilename().split("\\.")[1];
        String imagePath = FileSaver.saveFile(img, AppLauncher.uploadDirPath, imageExt);
        entry.setPreviewImage(imagePath);
        User author = userService.findByLogin(principal.getName());
        entry.setAuthor(author);
        ModelAndView modelAndView = new ModelAndView("entry_full");
        modelAndView.addObject(entry);
        return modelAndView;
    }

}
