package org.bitbucket.blog.controllers;

import org.bitbucket.blog.models.Comment;
import org.bitbucket.blog.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by Admin on 01.04.2016.
 */
@Controller
public class CommentController {

    @Autowired
    private CommentService commentService;

    @ResponseBody
    @RequestMapping(path = "/addComment", method = RequestMethod.POST)
    public void addComment(@ModelAttribute Comment comment) {
        commentService.create(comment);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @ResponseBody
    @RequestMapping(path = "/deleteComment", method = RequestMethod.DELETE)
    public void deleteComment(@RequestParam("id") Long id) {
        commentService.delete(id);
    }
}
