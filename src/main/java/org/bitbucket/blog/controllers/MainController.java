package org.bitbucket.blog.controllers;

import org.bitbucket.blog.models.Entry;
import org.bitbucket.blog.service.EntryService;
import org.bitbucket.blog.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import java.util.Set;

/**
 * Created by Anton on 12.03.2016.
 */

@Controller
public class MainController {

    private static final int ENTRIES_ON_PAGE = 10;

    @Autowired
    private EntryService entryService;
    @Autowired
    private TagService tagService;

    @RequestMapping("/main")
    public ModelAndView mainPage() {
        return mainPage(1, null);
    }

    @RequestMapping(value = "/main/page/{page}", method = RequestMethod.GET)
    public ModelAndView mainPage(@PathVariable("page") int page,
                                 @RequestParam(value = "criteria", required = false) String criteria) {
        ModelAndView modelAndView = new ModelAndView("main");
        Page<Entry> entries;
        page--; //пейджинация начинается с 0 индекса, а нумерация на ui начинается с 1
        if (criteria != null) {
            entries = entryService.findEntryByCriteriaPage(criteria, page, ENTRIES_ON_PAGE);
            modelAndView.addObject("criteria", criteria);
        } else {
            entries = entryService.findWithLimitPage(page, ENTRIES_ON_PAGE);
        }
        modelAndView.addObject("entries", entries);
        return modelAndView;
    }

    @RequestMapping("/thread/{id}")
    public ModelAndView entryFull(@PathVariable("id") long id) {
        ModelAndView modelAndView = new ModelAndView("entry_full");
        Entry entry = entryService.get(id);
        modelAndView.addObject("entry", entry);

        // для реализации рекурсивной Jsp-магии и вывода комментариев,
        // передаем их и подменяем в request scope в самой jsp
        Set comments = entry.getComments();
        modelAndView.addObject("comments", comments);
        return modelAndView;
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping("/admin_page")
    public ModelAndView adminPage() {
        ModelAndView modelAndView = new ModelAndView("admin_page");
        modelAndView.addObject("tags", tagService.getAll());
        return modelAndView;
    }

    @RequestMapping(value = "/tag", method = RequestMethod.GET)
    public ModelAndView tagSearch(@RequestParam("id") Long id) {
        ModelAndView modelAndView = new ModelAndView("main");
        modelAndView.addObject("entries", tagService.get(id).getEntries());
        return modelAndView;
    }
}
