package org.bitbucket.blog.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by Anton on 09.03.2016.
 */
@SuppressWarnings("ALL")
@Controller
public class LoginController {

    @RequestMapping("/index")
    private ModelAndView index() {
        return new ModelAndView("index");
    }

    @RequestMapping("/login")
    public String loginPage() {
        return "login";
    }
}
