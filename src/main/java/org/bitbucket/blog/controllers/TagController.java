package org.bitbucket.blog.controllers;

import org.bitbucket.blog.models.Tag;
import org.bitbucket.blog.models.dto.TagDto;
import org.bitbucket.blog.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

/**
 * Created by Admin on 02.04.2016.
 */
@Controller
public class TagController {

    @Autowired
    private TagService tagService;

    @PreAuthorize("hasAuthority('ADMIN')")
    @ResponseBody
    @RequestMapping(path = "/addTag", method = RequestMethod.POST)
    public Tag addComment(@ModelAttribute Tag tag) {
        return tagService.create(tag);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @ResponseBody
    @RequestMapping(path = "/deleteTag", method = RequestMethod.DELETE)
    public void deleteComment(@RequestParam("id") Long id) {
        tagService.delete(id);
    }


    @ResponseBody
    @RequestMapping(path = "/getAllTags",
            produces = {"application/xml", "application/json"})
    public List<TagDto> getAllTags() {
        return tagService.getAll().stream().map(this::tagConverter).collect(Collectors.toList());
    }

    private TagDto tagConverter(Tag tag) {
        final TagDto dto = new TagDto();
        dto.id = tag.getId();
        dto.name = tag.getName();
        return dto;
    }
}