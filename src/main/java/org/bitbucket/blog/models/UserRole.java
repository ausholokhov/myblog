package org.bitbucket.blog.models;

import org.bitbucket.blog.models.enums.UserRoleEnum;

import javax.persistence.*;

/**
 * Created by Anton on 27.03.2016.
 */
@Entity
@Table(name = "Role")
public class UserRole extends BaseEntity {

    private static final long serialVersionUID = 371656036406279435L;

    public UserRole(UserRoleEnum role) {
        this.role = role;
    }

    public UserRole() {
    }

    @Column(name = "title", unique = true, nullable = false)
    @Enumerated(EnumType.STRING)
    private UserRoleEnum role;

    public UserRoleEnum getRoleEnum() {
        return role;
    }

    public void setRoleEnum(UserRoleEnum role) {
        this.role = role;
    }

}
