package org.bitbucket.blog.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.SortedSet;

/**
 * Created by Anton on 13.03.2016.
 */
@Entity
@Table(name = "Comment")
public class Comment extends BaseEntity implements Comparable<Comment> {

    private static final long serialVersionUID = -4312392980123786301L;

    @Column(name = "author_name")
    @NotNull
    private String authorName;

    @Column(name = "author_email")
    @NotNull
    private String authorEmail;

    @Column(name = "body")
    @Lob
    @NotNull
    private String body;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "postDate")
    private Date postDate = new Date();

    @JoinColumn(name = "parent_id", referencedColumnName = "id")
    @ManyToOne(fetch = FetchType.EAGER)
    private Comment parentComment;

    @OneToMany(mappedBy = "parentComment", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE)
    @OrderBy("postDate")
    private SortedSet<Comment> reply;

    @ManyToOne
    @JoinColumn(name = "entry_id")
    private Entry entry;

    public Comment getParentComment() {
        return parentComment;
    }

    public void setParentComment(Comment parentComment) {
        this.parentComment = parentComment;
    }

    public Entry getEntry() {
        return entry;
    }

    public void setEntry(Entry entry) {
        this.entry = entry;
    }

    public String getAuthorName() {
        return authorName;
    }

    public void setAuthorName(String authorName) {
        this.authorName = authorName;
    }

    public String getAuthorEmail() {
        return authorEmail;
    }

    public void setAuthorEmail(String authorEmail) {
        this.authorEmail = authorEmail;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public SortedSet<Comment> getReply() {
        return reply;
    }

    public void setReply(SortedSet<Comment> reply) {
        this.reply = reply;
    }

    @Override
    public int compareTo(Comment comment) {
        return this.postDate.compareTo(comment.postDate);
    }
}
