package org.bitbucket.blog.models.enums;

/**
 * Created by Anton on 09.03.2016.
 */
public enum UserRoleEnum {

    ADMIN,
    USER,
    ANONYMOUS;

    UserRoleEnum() {
    }
}