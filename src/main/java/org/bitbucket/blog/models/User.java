package org.bitbucket.blog.models;

import javax.persistence.*;
import java.util.Collection;
import java.util.List;
import java.util.Set;

/**
 * Created by Anton on 09.03.2016.
 */
@Entity
@Table(name = "User")
public class User extends BaseEntity {

    private static final long serialVersionUID = 7176925554343757862L;

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "full_name")
    private String fullName;

    @OneToOne(fetch = FetchType.EAGER, targetEntity = UserRole.class, cascade = CascadeType.ALL)
    @JoinColumn(name = "role_id")
    private UserRole role;

    @OneToMany(mappedBy = "author", cascade = CascadeType.ALL)
    private List<Entry> entries;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    public Collection<Entry> getEntries() {
        return entries;
    }

    public void setEntries(List<Entry> entries) {
        this.entries = entries;
    }
}
