package org.bitbucket.blog.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;
import java.util.List;
import java.util.SortedSet;

/**
 * Created by Anton on 08.03.2016.
 */
@Entity
@Table(name = "Entry")
public class Entry extends BaseEntity {

    private static final long serialVersionUID = 5339223289991729471L;

    @Column(name = "header")
    @NotNull
    @Size(min = 1, max = 250)
    private String header;

    @Column(name = "short_description")
    @NotNull
    @Size(min = 1, max = 250)
    private String shortDescription;

    @Column(name = "preview_image")
    @NotNull
    private String previewImage;

    @Lob
    @Column(name = "full_description", columnDefinition = "TEXT")
    @NotNull
    @Size(min = 1)
    private String fullDescription;

    @ManyToOne
    @JoinColumn(name = "author_id")
    private User author;

    @Column(name = "postDate")
    @Temporal(TemporalType.TIMESTAMP)
    private Date postDate = new Date();

    @OneToMany(mappedBy = "entry", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @OrderBy("postDate")
    private SortedSet<Comment> comments;

    @ManyToMany(cascade = CascadeType.MERGE)
    @JoinTable(
            name = "entry_tags",
            joinColumns = @JoinColumn(name = "entry_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "tag_id", referencedColumnName = "id"))
    private List<Tag> tags;

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public String getPreviewImage() {
        return previewImage;
    }

    public void setPreviewImage(String previewImage) {
        this.previewImage = previewImage;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getFullDescription() {
        return fullDescription;
    }

    public void setFullDescription(String fullDescription) {
        this.fullDescription = fullDescription;
    }

    public User getAuthor() {
        return author;
    }

    public void setAuthor(User author) {
        this.author = author;
    }

    public Date getPostDate() {
        return postDate;
    }

    public void setPostDate(Date postDate) {
        this.postDate = postDate;
    }

    public SortedSet<Comment> getComments() {
        return comments;
    }

    public void setComments(SortedSet<Comment> comments) {
        this.comments = comments;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }
}
