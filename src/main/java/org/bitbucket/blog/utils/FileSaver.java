package org.bitbucket.blog.utils;

import org.apache.commons.io.FileUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

/**
 * Created by Admin on 30.03.2016.
 */
public class FileSaver {

    public static String saveFile(MultipartFile uploadFile, String directory, String ext) throws IOException {
        SimpleDateFormat dateFormat = new SimpleDateFormat("MM_dd_yyyy");
        String folder = dateFormat.format(new Date());
        String filePath = directory + folder;
        String pathAndName = filePath + "/" + UUID.randomUUID() + "." + ext;
        File file = new File(pathAndName);
        while (file.exists()) {
            pathAndName = filePath + UUID.randomUUID() + "." + ext;
            file = new File(pathAndName);
        }
        FileUtils.writeByteArrayToFile(file, uploadFile.getBytes());
        return pathAndName;
    }
}
