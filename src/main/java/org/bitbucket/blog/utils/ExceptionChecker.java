package org.bitbucket.blog.utils;

/**
 * Created by Anton on 12.03.2016.
 */
public class ExceptionChecker {

    private static final LoggerWrapper LOG = LoggerWrapper.get(ExceptionChecker.class);

    public static void check(boolean found, String msg) {
        if (!found) throw LOG.getNotFoundException("Not found entity with " + msg);
    }

    public static <T> T check(T object, long id) {
        return check(object, "id=" + id);
    }

    public static <T> T check(T object, String msg) {
        check(object != null, msg);
        return object;
    }
}
