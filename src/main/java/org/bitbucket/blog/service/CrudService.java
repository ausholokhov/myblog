package org.bitbucket.blog.service;

import java.util.List;

/**
 * Created by Anton on 12.03.2016.
 */
public interface CrudService<T> {
    T get(Long id);

    T create(T obj);

    T update(T obj);

    void delete(Long id);

    List<T> getAll();
}
