package org.bitbucket.blog.service;

import org.bitbucket.blog.models.Entry;
import org.springframework.data.domain.Page;

import java.util.List;

/**
 * Created by Anton on 12.03.2016.
 */
public interface EntryService extends CrudService<Entry> {

    Page<Entry> findWithLimitPage(int offset, int limit);

    Page<Entry> findEntryByCriteriaPage(String criteria, int offset, int limit);
}
