package org.bitbucket.blog.service;

import org.bitbucket.blog.models.Tag;

/**
 * Created by Admin on 02.04.2016.
 */
public interface TagService extends CrudService<Tag>{
}
