package org.bitbucket.blog.service;

import org.bitbucket.blog.models.Comment;

/**
 * Created by Admin on 01.04.2016.
 */
public interface CommentService extends CrudService<Comment> {
}
