package org.bitbucket.blog.service.impl;

import org.bitbucket.blog.models.User;
import org.bitbucket.blog.models.enums.UserRoleEnum;
import org.bitbucket.blog.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

/**
 * Created by Anton on 09.03.2016.
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String login) {
        User user = userService.findByLogin(login);
        Set<GrantedAuthority> authorities = new HashSet();
        authorities.add(new SimpleGrantedAuthority(user.getRole().getRoleEnum().name()));
        return new org.springframework.security.core.userdetails.User(user.getLogin(),
                user.getPassword(),
                authorities);
    }
}
