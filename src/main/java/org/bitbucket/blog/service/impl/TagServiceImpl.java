package org.bitbucket.blog.service.impl;

import org.bitbucket.blog.models.Tag;
import org.bitbucket.blog.repository.TagRepo;
import org.bitbucket.blog.service.TagService;
import org.bitbucket.blog.utils.ExceptionChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Admin on 02.04.2016.
 */
@Service
public class TagServiceImpl implements TagService {

    @Autowired
    private TagRepo repo;

    @Override
    public Tag get(Long id) {
        return ExceptionChecker.check(repo.findOne(id), id);
    }

    @Override
    public Tag create(Tag obj) {
        return repo.save(obj);
    }

    @Override
    public Tag update(Tag obj) {
        return ExceptionChecker.check(repo.save(obj), obj.getId());
    }

    @Override
    public void delete(Long id) {
        delete(id);
    }

    @Override
    public List<Tag> getAll() {
        return repo.findAll();
    }

}
