package org.bitbucket.blog.service.impl;

import org.bitbucket.blog.models.Comment;
import org.bitbucket.blog.repository.CommentRepo;
import org.bitbucket.blog.service.CommentService;
import org.bitbucket.blog.utils.ExceptionChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Admin on 01.04.2016.
 */
@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    private CommentRepo commentRepo;

    @Override
    public Comment get(Long id) {
        return ExceptionChecker.check(commentRepo.findOne(id), id);
    }

    @Override
    public Comment create(Comment obj) {
        return commentRepo.save(obj);
    }

    @Override
    public Comment update(Comment obj) {
        return ExceptionChecker.check(commentRepo.save(obj), obj.getId());
    }

    @Override
    public void delete(Long id) {
        commentRepo.delete(id);
    }

    @Override
    public List<Comment> getAll() {
        return commentRepo.findAll();
    }
}
