package org.bitbucket.blog.service.impl;


import org.bitbucket.blog.models.User;
import org.bitbucket.blog.repository.UserRepo;
import org.bitbucket.blog.service.UserService;
import org.bitbucket.blog.utils.ExceptionChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Anton on 12.03.2016.
 */
@Service
@Transactional
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepo userRepo;

    @Override
    public User findByLogin(String login) {
        return ExceptionChecker.check(userRepo.findByLogin(login), "login " + login);
    }

    @Override
    public User get(Long id) {
        return ExceptionChecker.check(userRepo.findOne(id), id);
    }

    @Override
    public User create(User obj) {
        return userRepo.save(obj);
    }

    @Override
    public User update(User obj) {
        return ExceptionChecker.check(userRepo.save(obj), obj.getId());
    }

    @Override
    public void delete(Long id) {
        userRepo.delete(id);
    }

    @Override
    public List<User> getAll() {
        return userRepo.findAll();
    }
}
