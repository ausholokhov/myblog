package org.bitbucket.blog.service.impl;

import org.bitbucket.blog.models.Entry;
import org.bitbucket.blog.repository.EntryRepo;
import org.bitbucket.blog.service.EntryService;
import org.bitbucket.blog.utils.ExceptionChecker;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by Anton on 12.03.2016.
 */
@Service
@Transactional
public class EntryServiceImpl implements EntryService {

    @Autowired
    private EntryRepo entryRepo;

    @Override
    public Entry get(Long id) {
        return ExceptionChecker.check(entryRepo.findOne(id), id);
    }

    @Override
    public Entry create(Entry obj) {
        return entryRepo.save(obj);
    }

    @Override
    public Entry update(Entry obj) {
        return ExceptionChecker.check(entryRepo.save(obj), obj.getId());
    }

    @Override
    public void delete(Long id) {
        entryRepo.delete(id);
    }

    @Override
    public List<Entry> getAll() {
        return entryRepo.findAll();
    }

    @Override
    public Page<Entry> findWithLimitPage(int offset, int limit) {
        return entryRepo.findAll(new PageRequest(offset, limit, Sort.Direction.DESC, "postDate"));
    }

    @Override
    public Page<Entry> findEntryByCriteriaPage(String criteria, int offset, int limit) {
        return entryRepo.findEntryByCriteriaLikePage(criteria, new PageRequest(offset, limit, Sort.Direction.DESC, "postDate"));
    }

}
