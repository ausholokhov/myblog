package org.bitbucket.blog.service;

import org.bitbucket.blog.models.User;

/**
 * Created by Anton on 12.03.2016.
 */
public interface UserService extends CrudService<User> {
    User findByLogin(String login);
}
