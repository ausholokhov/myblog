package org.bitbucket.blog.repository;

import org.bitbucket.blog.models.Tag;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Admin on 01.04.2016.
 */
public interface TagRepo extends JpaRepository<Tag, Long> {
}
