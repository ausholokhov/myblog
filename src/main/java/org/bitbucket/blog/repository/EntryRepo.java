package org.bitbucket.blog.repository;

import org.bitbucket.blog.models.Entry;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by Anton on 08.03.2016.
 */
public interface EntryRepo extends JpaRepository<Entry, Long> {

    @Query("SELECT e FROM org.bitbucket.blog.models.Entry e " +
            "WHERE e.header LIKE CONCAT('%',:criteria,'%') " +
            "OR e.shortDescription LIKE CONCAT('%',:criteria,'%')")
    Page<Entry> findEntryByCriteriaLikePage(@Param("criteria") String criteria, Pageable pageable);

    Page<Entry> findAll(Pageable pageable);
}
