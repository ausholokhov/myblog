package org.bitbucket.blog.repository;

import org.bitbucket.blog.models.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Anton on 09.03.2016.
 */
public interface UserRepo extends JpaRepository<User, Long> {
    User findByLogin(String name);
}
