<%@ attribute name="reply" required="true" type="org.bitbucket.blog.models.Comment" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="myTags" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<c:if test="${not empty reply}">
    <ul>
        <c:forEach var="comment" items="${reply.reply}">
            <li>${comment.authorName} ${comment.body}</li>

            <myTags:comments reply="${comment}"/>
        </c:forEach>
    </ul>
</c:if>
