<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" language="java" %>
<%--@elvariable id="entries" type="java.util.List<org.bitbucket.blog.models.Entry>"--%>


<!DOCTYPE html>
<html lang="en">

<%@include file="header.jsp" %>
<!-- Page Content -->
<%@include file="nav.jsp" %>
<div class="container">
    <div class="row">
        <!-- Blog Entries Column -->
        <div class="col-md-9">
            <br>
            <br>
            <h1 class="page-header">
                Страница администратора
            </h1>

            <form id="entryForm" method="POST" enctype="multipart/form-data" target="_blank" action="<c:url value="/preview"/>">
                <label>Картинка краткого описания:</label>
                <input type="hidden" name="previewImage" value="${entry.previewImage}">
                <c:if test="${entry!=null}">
                    <input type="hidden" name="postDate" value="${entry.postDate}">
                </c:if>
                <div class="input-group image-preview">
                    <input type="text" class="form-control image-preview-filename" disabled="disabled">
                    <!-- don't give a name === doesn't send on POST/GET -->
                <span class="input-group-btn">
                    <!-- image-preview-button button -->
                    <button type="button" class="btn btn-default image-preview-button" style="display:none;">
                        <span class="glyphicon glyphicon-picture"></span> Preview
                    </button>

                    <!-- image-preview-clear button -->
                    <button type="button" class="btn btn-default image-preview-clear" style="display:none;">
                        <span class="glyphicon glyphicon-remove"></span> Clear
                    </button>

                    <!-- image-preview-input -->
                    <div class="btn btn-default image-preview-input">
                        <span class="glyphicon glyphicon-folder-open"></span>
                        <span class="image-preview-input-title">Browse</span>
                        <input type="file" accept="image/png, image/jpeg, image/gif" name="image"/> <!-- rename it -->
                    </div>
                </span>
                </div><!-- /input-group image-preview [TO HERE]-->


                <br><label for="header">Теги:</label>
                <div>
                    <select multiple="multiple" name="tags" id="tags">

                        <c:forEach items="${entry.tags}" var="tag">
                            <option value="${tag.id}" selected>${tag.name}</option>
                        </c:forEach>

                        <c:forEach items="${tags}" var="tag">
                            <option value="${tag.id}">${tag.name}</option>
                        </c:forEach>
                    </select>
                    <button type="button" class="btn btn-info btn-md" data-toggle="modal" data-target="#createTag">
                        Создать тег
                    </button>
                </div>
                <br><label for="header">Заголовок новости:</label>
                <input type="hidden" name="id" value="${entry.id}">
            <textarea class="form-control vresize" rows="3" name="header"
                      id="header" maxlength="250">${entry.header}</textarea><br>

                <label for="shortDescription">Краткое описание:</label>
            <textarea class="form-control vresize" rows="3" name="shortDescription"
                      id="shortDescription" maxlength="250">${entry.shortDescription}</textarea>

                <br><label for="editor">Полное описание:</label><br>
                <textarea id="editor" name="fullDescription" rows='20' title="">${entry.fullDescription}</textarea>

                <button type="submit" id="previewEntryBtn" class="btn btn-primary">Предпросмотр</button>
                <button type="button" id="addEntryBtn" class="btn btn-success">Добавить пост</button>

            </form>
        </div>
        <br>
        <br>
        <br>

        <div class="modal fade" id="createTag" role="dialog">
            <div class="modal-dialog">
                <form role="form" id="addTagForm">
                    <!-- Modal content-->
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">&times;</button>
                            <h4 class="modal-title">Добавление нового тега</h4>
                        </div>
                        <div class="modal-body">
                            <label>Название тега</label>
                            <input name="name" id="tagName" class="form-control" type="text"/>
                        </div>
                        <div class="modal-footer">
                            <button id="saveNewTag" type="submit" class="btn btn-success" data-dismiss="modal">
                                Сохранить
                            </button>
                            <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <!-- Blog Sidebar Widgets Column -->
        <div class="col-md-3">
            <!-- Blog Search Well -->
            <div class="well">
                <h4>Blog Search</h4>
                <div class="input-group">
                    <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </span>
                </div>
                <!-- /.input-group -->
            </div>
            <!-- Blog Categories Well -->
            <div class="well">
                <h4>Blog Categories</h4>
                <div class="row">
                    <div class="col-lg-6">
                        <ul class="list-unstyled">
                            <li><a href="#">Category Name</a>
                            </li>
                            <li><a href="#">Category Name</a>
                            </li>
                            <li><a href="#">Category Name</a>
                            </li>
                            <li><a href="#">Category Name</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.col-lg-6 -->
                    <div class="col-lg-6">
                        <ul class="list-unstyled">
                            <li><a href="#">Category Name</a>
                            </li>
                            <li><a href="#">Category Name</a>
                            </li>
                            <li><a href="#">Category Name</a>
                            </li>
                            <li><a href="#">Category Name</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.col-lg-6 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- Side Widget Well -->
            <div class="well">
                <h4>Side Widget Well</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis
                    adipisci
                    accusamus
                    laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
            </div>
        </div>
    </div>
    <hr>
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>Все права защищены. © 2016 Антон Шолохов</p>
            </div>
        </div>
    </footer>
</div>

<form role="form" id="imageHiddenForm" enctype="multipart/form-data">
    <input type="file" accept="image/png, image/jpeg, image/gif" id="file1" name="img" style="display:none">
</form>

<script type="text/javascript" src="<c:url value="/js/multiselect.js"/>"></script>
<script src="<c:url value="/tinymce/tinymce.min.js"/>"></script>
<script src="<c:url value="/js/jquery.form.js"/>"></script>
<link rel="stylesheet" href="<c:url value="/css/multiselect.css"/>" type="text/css"/>


</html>


<script>
    $("#addEntryBtn").click(function () {
        var editorContent = tinyMCE.get('editor').getContent();
        $("#entryForm").attr("action", '<c:url value="/addEntry"/>');

        $("#editor").val(editorContent);
        $("#entryForm").ajaxForm({
            success: function () {
                swal('Новость добавлена', '', 'success');
            },
            error: function () {
                swal('Не сохранено', 'Во время сохранения произошла ошибка', 'error');
            },
            dataType: "text"
        }).submit();
    });

    function browser() {
        $("#file1").click();
    }

    $("#file1").change(function () {
        var formData = new FormData($('#imageHiddenForm')[0]);
        if (formData != null) {
            $.ajax({
                url: "/saveImage",
                type: "POST",
                dataType: 'text',
                contentType: false,
                processData: false,
                cache: false,
                data: formData,
                success: function (response) {
                    $("[id$='-inp']").attr("value", "image/" + response);
                },
                error: function () {
                    var message = 'Не удалось загрузить изображение';
                    $("[id$='-inp']").attr("value", message);
                }
            });
        }
    });

    tinymce.init({
        selector: '#editor',
        height: 500,
        language: 'ru',
        plugins: ["image, link, media",
            'advlist autolink lists link image charmap print preview anchor',
            'searchreplace visualblocks code fullscreen',
            'insertdatetime media table contextmenu paste code'
        ],
        file_picker_callback: browser,
        toolbar: 'sizeselect | bold italic | fontselect |  fontsizeselect | insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image',
    });
</script>

<script>
    $(document).ready(function () {
        $('#tags').multiselect();
    });

    $("#saveNewTag").click(function () {
        $.ajax({
            type: "POST",
            url: '<c:url value="/addTag"/>',
            data: $("#addTagForm").serialize(),
            dataType: 'text',
            timeout: 100000,
            success: function (data) {
                var tag = jQuery.parseJSON(data);
                var elementView = '<li><a href="javascript:void(0);"><label class="checkbox"><input type="checkbox" value="' + tag.id + '">' + tag.name + '</label></a></li>';
                var elementOptions = '<option value="' + tag.id + '" data-icon="glyphicon-tag">' + tag.name + '</option>';
                $("#multiselectMenu").append(elementView);
                $("#tags").append(elementOptions);
                $('#tags').multiselect('rebuild');
            },
            error: function () {
                swal('Изменения не сохранены', 'Во время сохранения произошла ошибка', 'error');
            }
        })
    });

    $(document).ready(function () {
        // Set the close button
        var closebtn = $('<button/>', {
            type: "button",
            text: 'x',
            id: 'close-preview',
            style: 'font-size: initial;',
        });
        closebtn.attr("class", "close pull-right");
        closebtn.attr("onclick", "closePreview();");
        // Set the popover default content
        $('.image-preview').popover({
            trigger: 'manual',
            html: true,
            title: "<strong>Preview</strong>" + $(closebtn)[0].outerHTML,
            content: 'Loading...',
            placement: 'bottom'
        });
        // Set the clear onclick function
        $('.image-preview-clear').click(function () {
            $('.image-preview').popover('hide');
            $('.image-preview-filename').val("");
            $('.image-preview-clear').hide();
            $('.image-preview-button').hide();
            $('.image-preview-input input:file').val("");
            $(".image-preview-input-title").text("Browse");
        });

        $('.image-preview-button').on('click', function () {
            $('.image-preview').popover('toggle');
        });
    });

    function closePreview() {
        $('.image-preview').popover('hide');
    }

    $(function () {
        var imgsrc = '<c:out value="${entry.previewImage}"/>';
        if (imgsrc) {
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250,
                height: 250,
                src: '/image/' + imgsrc
            });
            $(".image-preview-input-title").text("Change");
            $(".image-preview-button").show();
            $(".image-preview-filename").val(imgsrc);
            $(".image-preview").attr("data-content", $(img)[0].outerHTML);
        }
    });

    $(function () {
        $(".image-preview-input input:file").change(function () {
            // Create the preview image
            var img = $('<img/>', {
                id: 'dynamic',
                width: 250,
                height: 250
            });
            var file = this.files[0];
            var reader = new FileReader();
            reader.onload = function (e) {
                $(".image-preview-input-title").text("Change");
                $(".image-preview-clear").show();
                $(".image-preview-button").show();
                $(".image-preview-filename").val(file.name);
                // Set preview image into the popover data-content
                img.attr('src', e.target.result);
                $(".image-preview").attr("data-content", $(img)[0].outerHTML).popover("show");
            };
            reader.readAsDataURL(file);
        });
    });
</script>

<style>
    .vresize {
        resize: vertical;
    }

    .image-preview-input {
        position: relative;
        overflow: hidden;
        margin: 0px;
        color: #333;
        background-color: #fff;
        border-color: #ccc;
    }

    .image-preview-input input[type=file] {
        position: absolute;
        top: 0;
        right: 0;
        margin: 0;
        padding: 0;
        font-size: 20px;
        cursor: pointer;
        opacity: 0;
        filter: alpha(opacity=0);
    }

    .image-preview-input-title {
        margin-left: 2px;
    }
</style>

