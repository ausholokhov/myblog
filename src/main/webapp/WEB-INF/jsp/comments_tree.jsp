<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="myTags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" language="java" %>
<%--@elvariable id="comments" type="java.util.List<org.bitbucket.blog.models.Comment>"--%>
<%--@elvariable id="comment" type="org.bitbucket.blog.models.Comment"--%>

<%--Вы входите в зону пониженного комфорта. --%>
<%--Пожалуйста покиньте данный код, и больше не возвращайтесь.--%>
<%--Вы все еще здесь? Узрите же jsp рекурсию для отображения дерева комментариев...--%>

<!DOCTYPE html>
<html lang="en">
<ul>
    <c:forEach var="comment" items="${comments}" varStatus="index">
        <c:if test="${comment.parentComment==null || isParentComment==true}">
            <c:set var="isParentComment" value="true" scope="request"/>
            <div class="message-item">
                <div class="message-inner">
                    <div class="message-head clearfix">
                        <div class="user-detail">
                            <div style="width: 100%; height: 30px; line-height: 30px; overflow: hidden; text-align: justify;">
                            <span style="display: inline-block;">
                            <i class="glyphicon glyphicon-user"></i> ${comment.authorName}</span>
                        <span style="display: inline-block;">
                             <i class="glyphicon glyphicon-calendar"></i>
                                <fmt:formatDate value="${comment.postDate}" pattern="dd.MM.yyyy"/>
                                &nbsp
                                <i class="glyphicon glyphicon-time"></i>
                                <fmt:formatDate value="${comment.postDate}" pattern="HH:mm"/>
                        </span>
                                <span style="letter-spacing: 5000px;">&nbsp;&nbsp;</span>
                            </div>
                        </div>
                    </div>
                    <div class="qa-message-content">
                            ${comment.body}
                    </div>
                    <hr>
                    <button type="button" id="replyButton${comment.id}" class="btn main-default btn-xs"
                            onclick="addCommentForm(${comment.id})">Ответить
                    </button>
                    <sec:authorize access="hasAnyAuthority('ADMIN')">
                        <button type="button" onclick="deleteComment(${comment.id})" class="btn btn-danger btn-xs"
                                title="Удалить запись">
                            Удалить
                        </button>
                    </sec:authorize>
                    <div id="reply${comment.id}"></div>
                </div>
            </div>
            <c:set var="comments" value="${comment.reply}" scope="request"/>
            <jsp:include page="comments_tree.jsp"/>
            <c:set var="isParentComment" value="false" scope="request"/>

            <c:if test="${comment.parentComment != null}">
                <c:set var="isParentComment" value="true" scope="request"/>
                <c:set var="comments" value="${comment.parentComment}" scope="request"/>
            </c:if>
        </c:if>
    </c:forEach>
</ul>
</html>
