<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" language="java" %>
<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse"
                    data-target="#bs-example-navbar-collapse-1">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="<c:url value="/main"/>">Главная</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
                <li>
                    <a href="#">О блоге</a>
                </li>
                <li>
                    <a href="#">Контакты</a>
                </li>
                <sec:authorize access="hasAuthority('ADMIN')">
                    <li>
                        <a href="${pageContext.request.contextPath}/admin_page">Администрирование</a>
                    </li>
                </sec:authorize>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <sec:authorize access="isAnonymous()">
                    <li><a href="<c:url value="#"/>"><span
                            class="glyphicon glyphicon-user"></span> Зарегистрироваться</a></li>
                    <li><a href="<c:url value="/login"/>"><span
                            class="glyphicon glyphicon-log-in"></span> Войти</a></li>
                </sec:authorize>
                <sec:authorize access="isAuthenticated()">
                    <sec:authentication var="user" property="principal"/>
                    <li><a><span class="glyphicon glyphicon-user"></span> ${user.username}</a></li>
                    <li><a href="<c:url value="/logout"/>"><span
                            class="glyphicon glyphicon-log-in"></span> Выйти</a></li>
                </sec:authorize>
            </ul>
        </div>
        <!-- /.navbar-collapse -->
    </div>
    <!-- /.container -->
</nav>