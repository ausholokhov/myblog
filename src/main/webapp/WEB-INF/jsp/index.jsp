<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="sec" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Bootstrap core CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <!-- jQuery library -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <!-- Latest compiled JavaScript -->
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</head>


<div class="container">

    <div class="jumbotron" style="margin-top: 20px;">
        <h1>blog.com</h1>
        <p class="lead">
            Внезапная бессмысленная строка, чтобы было не скучно.
        </p>
        <sec:authorize access="!isAuthenticated()">
            <p><a class="btn btn-lg btn-success" href="<c:url value="/login" />" role="button">Войти</a></p>
        </sec:authorize>
        <sec:authorize access="isAuthenticated()">
            <p>Ваш логин: <sec:authentication property="principal.username" /></p>
            <p>Права: <sec:authentication property="principal.authorities" /></p>

            <p>Тест админа: <sec:authorize  access="hasAuthority('ADMIN')"> Видит только админ </sec:authorize> </p>

            <p><a class="btn btn-lg btn-danger" href="<c:url value="/logout" />" role="button">Выйти</a></p>

            <p><a class="btn btn-lg btn-danger" href="<c:url value="/main" />" role="button">На главную</a></p>

        </sec:authorize>
    </div>

    <div class="footer">
        <p>Blog 2014</p>
    </div>

</div>
</html>