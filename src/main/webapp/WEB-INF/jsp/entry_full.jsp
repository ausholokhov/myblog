<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib tagdir="/WEB-INF/tags" prefix="myTags" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" language="java" %>
<%--@elvariable id="entry" type="org.bitbucket.blog.models.Entry"--%>

<!DOCTYPE html>
<html lang="en">
<%@include file="header.jsp" %>
<%@include file="nav.jsp" %>

<!-- Page Content -->
<br>
<br>
<br>
<div class="container">
    <div class="row">
        <!-- Blog Entries Column -->
        <div class="col-md-9">
            <div class="well" style="background: white">
                <h2>
                    <strong>${entry.header}</strong>
                </h2>
                ${entry.fullDescription}
                <hr>
                <div class="buttom-post-info">
                    <%--<wtf>--%>
                    <div style="width: 100%; height: 30px; line-height: 30px; overflow: hidden; text-align: justify;">
                        <span style="display: inline-block;">
                             <i class="glyphicon glyphicon-calendar"></i>
                                <fmt:formatDate value="${entry.postDate}" pattern="dd.MM.yyyy"/>
                                &nbsp
                                <i class="glyphicon glyphicon-time"></i>
                                <fmt:formatDate value="${entry.postDate}" pattern="HH:mm"/>
                        </span>
                        <span style="display: inline-block;">
                            <i class="glyphicon glyphicon-user"></i> ${entry.author.fullName}</span>
                        <span style="letter-spacing: 5000px;">&nbsp;&nbsp;</span>
                    </div>
                    <%--</wtf>--%>
                </div>
            </div>

            <div class="well" style="background: white">
                <button id="addNewComment" type="submit" class="btn main-btn" onclick="addCommentForm()">
                    Добавить комментарий
                </button>
                <div id="newComment"></div>
                <h3><strong>Комментариев:</strong>
                    <span class="text-muted">${entry.comments.size()}</span>
                </h3>
                <jsp:include page="comments_tree.jsp"/>
            </div>
        </div>

        <!-- Blog Sidebar Widgets Column -->
        <div class="col-md-3">
            <!-- Blog Search Well -->
            <div class="well">
                <h4>Blog Search</h4>
                <div class="input-group">
                    <input type="text" class="form-control">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </span>
                </div>
                <!-- /.input-group -->
            </div>
            <!-- Blog Categories Well -->
            <div class="well">
                <h4>Blog Categories</h4>
                <div class="row">
                    <div class="col-lg-6">
                        <ul class="list-unstyled">
                            <li><a href="#">Category Name</a>
                            </li>
                            <li><a href="#">Category Name</a>
                            </li>
                            <li><a href="#">Category Name</a>
                            </li>
                            <li><a href="#">Category Name</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.col-lg-6 -->
                    <div class="col-lg-6">
                        <ul class="list-unstyled">
                            <li><a href="#">Category Name</a>
                            </li>
                            <li><a href="#">Category Name</a>
                            </li>
                            <li><a href="#">Category Name</a>
                            </li>
                            <li><a href="#">Category Name</a>
                            </li>
                        </ul>
                    </div>
                    <!-- /.col-lg-6 -->
                </div>
                <!-- /.row -->
            </div>
            <!-- Side Widget Well -->
            <a href="http://www.accuweather.com/en/ru/khabarovsk/293149/weather-forecast/293149"
               class="aw-widget-legal">
            </a>
            <div id="awcc1459744376030" class="aw-widget-current" data-locationkey="" data-unit="c" data-language="ru"
                 data-useip="true" data-uid="awcc1459744376030"></div>
            <script type="text/javascript" src="http://oap.accuweather.com/launch.js"></script>
        </div>
    </div>
    <hr>
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>Все права защищены. © 2016 Антон Шолохов</p>
            </div>
        </div>
    </footer>
</div>

</html>

<script>
    function deleteComment(id) {
        swal({
            title: 'Удалить комментарий и все ответы?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Да',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'confirm-class',
            cancelButtonClass: 'cancel-class',
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "DELETE",
                    url: '<c:url value="/deleteComment?id="/>' + id,
                    dataType: 'text',
                    timeout: 100000,
                    success: function () {
                        swal('Удалено', '', 'success');
                        setTimeout(function () {
                           location.reload();
                        }, 1000);
                    },
                    error: function () {
                        swal('Удаление отменено', 'Во время удаления произошла ошибка', 'error');
                    }
                })
            } else {
                swal('Удаление отменено', '', 'error');
            }
        });
    }

    function addCommentForm(id) {
        if (!id) {
            id = ''
        }
        var form = '<form role="form" id="commentForm' + id + '" class="contact-form" ">\
                <input type="hidden" id ="parentId" name="parentComment.id" value="' + id + '">\
                <input type="hidden" id = "entryId" name="entry.id" value="' + ${entry.id} +'">\
                <div class="row">\
                <div class="col-md-6">\
                <div class="form-group">\
                <input type="text" class="form-control cmt" name="authorName" autocomplete="off" id="authorName"\
        placeholder="Имя" maxlength="20" required>\
        </div>\
        </div>\
        <div class="col-md-6">\
                <div class="form-group">\
                <input type="email" class="form-control cmt" name="authorEmail" autocomplete="off" id="authorEmail"\
        placeholder="E-mail (не будет опубликован)" maxlength="30" required>\
        </div>\
        </div>\
        </div>\
        <div class="row">\
                <div class="col-md-12">\
                <div class="form-group">\
                <textarea class="form-control textarea cmt" rows="3" name="body" id="body"\
        placeholder="Комментарий" required></textarea>\
        </div>\
        </div>\
        </div>\
        <div class="row">\
                </div>\
                <button type="button" onclick="addComment(' + id + ')" class="btn main-btn">Отправить</button>\
                </form>';
        $(".contact-form").remove();
        if (id == '') {
            $("#newComment").html(form);
            $("#parentId").remove();
        } else {
            $("#reply" + id).html(form);
        }
    }

    function addComment(id) {
        if (!id){
            id = '';
        }
        $.ajax({
            type: "POST",
            url: '<c:url value="/addComment"/>',
            data: $("#commentForm" + id).serialize(),
            dataType: 'text',
            timeout: 100000,
            success: function () {
                location.reload();
            },
            error: function () {
                swal('Изменения не сохранены', 'Во время сохранения произошла ошибка', 'error');
            }
        })
    }

</script>

<style>
    .contact-form {
        margin-top: 15px;
    }

    .contact-form .textarea {
        min-height: 220px;
        resize: none;
    }

    .cmt {
        box-shadow: none;
        border-color: #eee;
        height: 49px;
    }

    .cmt:focus {
        box-shadow: none;
        border-color: #00b09c;
    }

    .main-btn {
        background: #00b09c;
        border-color: #00b09c;
        color: #fff;
    }

    .main-btn:hover {
        background: #00a491;
        color: #fff;
    }

    .message-item {
        margin-bottom: 5px;
        margin-left: -40px;
        position: relative;
    }

    .message-item .message-inner {
        background: #fff;
        border: 1px solid #ddd;
        border-radius: 3px;
        padding: 10px;
        position: relative;
    }

    .message-item .message-inner:before {
        border: 10px solid;
        border-right-color: #ddd;
        color: rgba(0, 0, 0, 0);
        content: "";
        display: block;
        height: 0;
        position: absolute;
        left: -20px;
        top: 6px;
        width: 0;
    }

    .message-item .message-inner:after {
        border: 10px solid;
        border-right-color: #fff;
        color: rgba(0, 0, 0, 0);
        content: "";
        display: block;
        height: 0;
        position: absolute;
        left: -18px;
        top: 6px;
        width: 0;
    }

    .message-item .message-head {
        border-bottom: 1px solid #eee;
        margin-bottom: 8px;
        padding-bottom: 8px;
    }

    .message-item .message-head .avatar {
        margin-right: 20px;
    }

    .message-item .message-head .user-detail {
        overflow: hidden;
    }

    .message-item .message-head .user-detail h5 {
        font-size: 16px;
        font-weight: bold;
        margin: 0;
    }

    .message-item .message-head .post-meta {
        float: left;
        padding: 0 15px 0 0;
    }

    .message-item .message-head .post-meta > div {
        color: #333;
        font-weight: bold;
        text-align: right;
    }

    .post-meta > div {
        color: #777;
        font-size: 12px;
        line-height: 22px;
    }

    .message-item .message-head .post-meta > div {
        color: #333;
        font-weight: bold;
        text-align: right;
    }

    .post-meta > div {
        color: #777;
        font-size: 12px;
        line-height: 22px;
    }

</style>