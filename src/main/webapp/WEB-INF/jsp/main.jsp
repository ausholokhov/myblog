<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page contentType="text/html" pageEncoding="UTF-8" language="java" %>
<%--@elvariable id="entries" type="org.springframework.data.domain.Page<org.bitbucket.blog.models.Entry>"--%>

<!DOCTYPE html>
<html lang="en">

<%@include file="header.jsp" %>
<%@include file="nav.jsp" %>

<!-- Page Content -->
<div class="container">
    <div class="row">
        <!-- Blog Entries Column -->
        <div class="col-md-9">
            <br>
            <br>
            <h1 class="page-header">
                Блог о Java:
                <small>статьи | переводы</small>
            </h1>
            <c:choose>
                <c:when test="${entries.content.size()==0}">
                    <h4>Ничего не найдено</h4>
                </c:when>
                <c:otherwise>
                    <c:forEach items="${entries.content}" var="entry">
                        <div class="well" style="background: white" id="">
                            <sec:authorize access="hasAnyAuthority('ADMIN','USER')">
                                <div align="right">
                                    <button type="button" class="btn btn-success btn-xs" name="deleteEntry"
                                            onclick="upEntry(${entry.id})" title="Апнуть">
                                        <span class="glyphicon glyphicon-arrow-up"></span>
                                    </button>
                                    <a href="<c:url value='/editEntry?id=${entry.id}'/>" class="btn btn-warning btn-xs">
                                        <span class="glyphicon glyphicon-pencil" title="Отредактировать"></span>
                                    </a>
                                    <button type="button" class="btn btn-danger btn-xs" name="deleteEntry"
                                            onclick="deleteEntry(${entry.id})" title="Удалить запись">
                                        <span class="glyphicon glyphicon-remove"></span>
                                    </button>
                                </div>
                            </sec:authorize>
                            <div class="entry_left">
                                <div class="entry_right entry_image">
                                    <img width="120" height="120" src="/image/${entry.previewImage}" alt="logo"/>
                                </div>
                                <div class="entry_right entry_text">
                                    <div class="span10">
                                        <h3><a href="/thread/${entry.id}">${entry.header}</a></h3>
                                        <div><span class="text-muted"><strong>Автор: </strong>${entry.author.fullName}</span></div>
                                        <c:if test="${entry.tags.size()!=0}">
                                            <c:forEach items="${entry.tags}" var="tag">
                                                <a class="btn btn-default btn-xs"
                                                   href="<c:url value="/tag?id=${tag.id}"/>">
                                                    <span class="glyphicon glyphicon-tag"></span>${tag.name}</a>
                                            </c:forEach>
                                        </c:if>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <p>${entry.shortDescription}</p>
                            <hr>
                            <div class="buttom-post-info">
                                <i class="glyphicon glyphicon-calendar"></i>
                                <fmt:formatDate value="${entry.postDate}" pattern="dd-MM-yyyy"/>
                                &nbsp
                                <i class="glyphicon glyphicon-time"></i>
                                <fmt:formatDate value="${entry.postDate}" pattern="HH:mm"/>
                            </div>
                        </div>

                    </c:forEach>

                    <!-- Pager -->
                    <c:choose>
                        <c:when test="${criteria!=null}">
                            <ul class="pagination">
                                <c:forEach begin="1" end="${entries.totalPages}" var="page">
                                    <li><a href="/main/page/${page}?criteria=${criteria}">${page}</a></li>
                                </c:forEach>
                            </ul>
                        </c:when>
                        <c:otherwise>
                            <ul class="pagination">
                                <c:forEach begin="1" end="${entries.totalPages}" var="page">
                                    <li><a href="/main/page/${page}">${page}</a></li>
                                </c:forEach>
                            </ul>
                        </c:otherwise>
                    </c:choose>
                </c:otherwise>
            </c:choose>
        </div>
        <br>
        <br>
        <br>
        <!-- Blog Sidebar Widgets Column -->
        <div class="col-md-3">
            <!-- Blog Search Well -->
            <div class="well">
                <h4>Поиск</h4>
                <div class="input-group">
                    <input type="text" class="form-control" id="searchInput" onkeydown="if (event.keyCode == 13)
                        $('#searchButton').click()">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="button" id="searchButton">
                                <span class="glyphicon glyphicon-search"></span>
                            </button>
                        </span>
                </div>
                <!-- /.input-group -->
            </div>
            <!-- Blog Categories Well -->
            <div class="well">
                <h4>Теги</h4>
                <div class="row">
                    <div class="col-lg-12" align="center" id="categories"></div>
                </div>
                <!-- /.row -->
            </div>
            <!-- Side Widget Well -->
            <a href="http://www.accuweather.com/en/ru/khabarovsk/293149/weather-forecast/293149"
               class="aw-widget-legal">
            </a>
            <div id="awcc1459744376030" class="aw-widget-current" data-locationkey="" data-unit="c" data-language="ru"
                 data-useip="true" data-uid="awcc1459744376030"></div>
            <script type="text/javascript" src="http://oap.accuweather.com/launch.js"></script>
        </div>
    </div>
    <hr>
    <footer>
        <div class="row">
            <div class="col-lg-12">
                <p>Все права защищены. © 2016 Антон Шолохов</p>
            </div>
        </div>
    </footer>
</div>
</html>

<script>
    $(document).ready(function () {
        $.ajax({
            type: "GET",
            url: '<c:url value="/getAllTags"/>',
            dataType: "json",
            success: function (data) {
                $.map(data, function (tag) {
//                    var color = "#" + ((1 << 24) * Math.random() | 0).toString(16);
                    var category = '<a color class="btn btn-default btn-xs"\
                    href="<c:url value="#"/>"> <span align="center" class="glyphicon glyphicon-tag"></span>' + tag.name + '</a>';
                    $("#categories").append(category);
                });
            }
        });
    });

    $('#searchButton').click(function () {
        var criteria = $("#searchInput").val();
        if (criteria != "") {
            window.location.href = "/main/page/1?criteria=" + criteria;
        }
    });

    function deleteEntry(entryId) {
        swal({
            title: 'Удалить запись?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Да',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'confirm-class',
            cancelButtonClass: 'cancel-class',
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "DELETE",
                    url: '<c:url value="/deleteEntry?id="/>' + entryId,
                    dataType: 'text',
                    timeout: 100000,
                    success: function () {
                        swal('Удалено', '', 'success');
                        setTimeout(function () {
                            location.reload();
                        }, 1000);
                    },
                    error: function () {
                        swal('Удаление отменено', 'Во время удаления произошла ошибка', 'error');
                    }
                })
            } else {
                swal('Удаление отменено', '', 'error');
            }
        });
    }

    function upEntry(entryId) {
        swal({
            title: 'Апнуть запись?',
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Да',
            cancelButtonText: 'Отмена',
            confirmButtonClass: 'confirm-class',
            cancelButtonClass: 'cancel-class',
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                $.ajax({
                    type: "DELETE",
                    url: '<c:url value="/upEntry?id="/>' + entryId,
                    dataType: 'text',
                    timeout: 100000,
                    success: function () {
                        swal('Готово', '', 'success');
                        setTimeout(function () {
                            location.reload();
                        }, 1000);
                    },
                    error: function () {
                        swal('Отменено', 'Во время операции произошла ошибка', 'error');
                    }
                })
            } else {
                swal('Отменено', '', 'error');
            }
        });
    }
</script>

<style>

    .entry_left {
        width: 100%;
        overflow: hidden;
    }

    .entry_right {
        height: 100%;
    }

    .entry_image {
        float: left;
        width: 18%
    }

    .entry_text {
        float: left;
        width: 82%
    }
</style>

