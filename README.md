## Java Blog ##

**Разработал функциональность:**

* авторизация и ролирование (admin/user) при помощи Spring Security
* добавление, удаление, редактирование статей через страницу администратора
* сохранение изображений из статей в файловою систему (в доработке)
* отображение кратких версий статей с пейджинацией от новых к старым (сортирует статьи по дате и времени)
* администратор может "апнуть" статью специальной кнопкой (для post_time статьи установится текущее время)
* отображение полных версий статей
* возможность комментирования статей (комментарии отображаются в виде дерева)
* поиск по статьям
* боковая панель с тегами заполняется динамически (AJAX запрос получает все теги из базы и добавляет в боковую панель)

**Инструменты:**
JDK 8, Spring Boot/MVC/Security/Data, Hibernate, jQuery, AJAX, Sweet Notes, Bootstrap, Jackson, JSP, JSTL, MySQL, Apache Maven, Apache Tomcat, Apache Commons, HikariCP, TinyMCE.

**Планирую:**

* удаление изображений к статьям из файловой системы при удалении статьи
* отображение статей по нажатию на тег

**Notes:**

![d22513b02dcd34b082e70835de33de77.png](https://bitbucket.org/repo/dz7dRo/images/3982105372-d22513b02dcd34b082e70835de33de77.png)


**Screenshots:**

![screencapture-localhost-8080-main-1461461959718.png](https://bitbucket.org/repo/dz7dRo/images/2865717868-screencapture-localhost-8080-main-1461461959718.png)
![screencapture-localhost-8080-103-1461484853906.png](https://bitbucket.org/repo/dz7dRo/images/2008132049-screencapture-localhost-8080-103-1461484853906.png)
![screencapture-localhost-8080-admin_page-1461461757809.png](https://bitbucket.org/repo/dz7dRo/images/1713845883-screencapture-localhost-8080-admin_page-1461461757809.png)

**Sholokhov Anton**